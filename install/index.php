<?php

use \Bitrix\Highloadblock as HL;

global $MESS;
IncludeModuleLangFile(__FILE__);

class kingbird_redirect extends CModule
{
    public $MODULE_ID = 'kingbird.redirect';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;
    public $MODULE_GROUP_RIGHTS = 'Y';
    public $NEED_MAIN_VERSION = '';
    public $NEED_MODULES = array();

    public function __construct() {

        $arModuleVersion = array('VERSION' => '1.0.0');
        include('version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->PARTNER_NAME = GetMessage("KBR_PARTNER_NAME");
        $this->PARTNER_URI = 'http://www.kingbird.ru/';

        $this->MODULE_NAME = GetMessage('KBR_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('KBR_MODULE_DESCRIPTION');
    }

    public function DoInstall() {
        if (is_array($this->NEED_MODULES) && !empty($this->NEED_MODULES)) {
            foreach ($this->NEED_MODULES as $module) {
                if (!IsModuleInstalled($module)) {
                    $this->ShowForm('ERROR', GetMessage('KBR_NEED_MODULES', array('#MODULE#' => $module)));
                }
            }
        }

        if (!$this->DoInstallDB()) {
            $this->ShowForm('ERROR', GetMessage('KBR_NEED_RIGHT_VER', array('#NEED#' => $this->LAST_ERROR)));
        }

        if (strlen($this->NEED_MAIN_VERSION)<=0 || version_compare(SM_VERSION, $this->NEED_MAIN_VERSION)>=0) {
            //RegisterModuleDependences('iblock', 'OnBeforeIBlockSectionUpdate', $this->MODULE_ID, 'CDifferenceHandlers', 'beforeIBSection');

            RegisterModule($this->MODULE_ID);
            $this->ShowForm('OK', GetMessage('MOD_INST_OK'));
        } else {
            $this->ShowForm('ERROR', GetMessage('KBR_NEED_RIGHT_VER', array('#NEED#' => $this->NEED_MAIN_VERSION)));
        }
    }

    public function DoInstallDB () {
        $arIblockProperties = array(
            'UF_FROM' => array (
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '300',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'S',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'Y',
                'SETTINGS' => array (
                    'SIZE' => 50,
                    'ROWS' => 1,
                    'REGEXP' => '',
                    'MIN_LENGTH' => 0,
                    'MAX_LENGTH' => 0,
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'ru' => 'from',
                    'en' => 'from',
                )

            ),
            'UF_TO' => array (
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '300',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'S',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'Y',
                'SETTINGS' => array (
                    'SIZE' => 50,
                    'ROWS' => 1,
                    'REGEXP' => '',
                    'MIN_LENGTH' => 0,
                    'MAX_LENGTH' => 0,
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'ru' => 'to',
                    'en' => 'to',
                )

            ),
        );

        if($this->_createHLblock(array(
            'NAME' => "HLRedirect",
            'TABLE_NAME' => 'hl_redirect',
        ))){
            foreach($arIblockProperties as $propName => $hlProperty){
                $hlProperty['FIELD_NAME'] = $propName;
                if(!$this->_createHLProperty($hlProperty)){
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    protected function _createHLblock($arItem) {
        if (strlen($arItem['TABLE_NAME']) <= 0 || strlen($arItem['NAME']) <= 0) {
            return false;
        } else {
            $this->LAST_ERROR = "Не указано название таблицы или ее имя";
        }

        $this->arHlBlock = $arItem;
        $rsHLBlock = HL\HighloadBlockTable::getList(array(
            'filter' => array('TABLE_NAME' => $arItem['TABLE_NAME']),
            'select' => array('ID')
        ))->fetch();
        $this->arHlBlock['ID'] = $rsHLBlock['ID'];

        if ($rsHLBlock['ID']) {
            $this->arHlBlock['ID'] = $rsHLBlock['ID'];
            return $rsHLBlock['ID'];
        }

        $rsResultAdd = HL\HighloadBlockTable::add($arItem);
        if ($rsResultAdd->isSuccess()) {
            $this->arHlBlock['ID'] = $rsResultAdd->getId();
            return $rsResultAdd->getId();
        } else {
            $this->LAST_ERROR = $rsResultAdd->getErrorMessages();
            return false;
        }
    }

    protected function _createHLProperty($arFields, $idIblock = 0) {
        $obFieldsEntityHL = new CUserTypeEntity();
        $idIblock = intval($idIblock) > 0 ? intval($idIblock) : $this->arHlBlock['ID'];

        if (strlen($arFields['FIELD_NAME']) > 0 && intval($idIblock) > 0) {
            $arFields['ENTITY_ID'] = 'HLBLOCK_' . $idIblock;

            $idEntity = false;
            $rsEntityProp = CUserTypeEntity::GetList(array(), array('ENTITY_ID' => $arFields['ENTITY_ID'], 'FIELD_NAME' => $arFields['FIELD_NAME']));
            if ($arEntityProp = $rsEntityProp->Fetch()) {
                if ($bResultAdd = $obFieldsEntityHL->update($arEntityProp['ID'], $arFields)) {
                    $idEntity = $arEntityProp['ID'];
                }
            } else {
                $idEntity = $obFieldsEntityHL->add($arFields);
            }

            if ($idEntity <= 0) {
                $this->LAST_ERROR = "Ошибка добавления/изменения свойства {$arFields['FIELD_NAME']} " . $obFieldsEntityHL->LAST_ERROR;
            } else {
                return $idEntity;
            }
        } else {
            $this->LAST_ERROR = "Ошибка FIELD_NAME=\"{$arFields['FIELD_NAME']}\" HL_BLOCK=\"$idIblock\" ";
        }
        return false;
    }

    public function DoUninstall() {
        //UnRegisterModuleDependences('iblock', 'OnBeforeIBlockSectionUpdate', $this->MODULE_ID, 'CDifferenceHandlers', 'beforeIBSection');
        if (!$this->DoUninstallDB()) {
            $this->ShowForm('ERROR', GetMessage('KBR_NEED_RIGHT_VER', array('#NEED#' => $this->LAST_ERROR)));
            return false;
        }

        UnRegisterModule($this->MODULE_ID);
        $this->ShowForm('OK', GetMessage('MOD_UNINST_OK'));
    }

    public function DoUninstallDB() {
        return $this->_deleteHLblock(array(
            'NAME' => "HLRedirect",
            'TABLE_NAME' => 'hl_redirect',
        ));
    }

    protected function _deleteHLblock($arItem) {
        if (strlen($arItem['TABLE_NAME']) <= 0 || strlen($arItem['NAME']) <= 0) {
            return false;
        } else {
            $this->LAST_ERROR = "Не указано название таблицы или ее имя";
        }

        $rsHLBlock = HL\HighloadBlockTable::getList(array(
            'filter' => array('TABLE_NAME' => $arItem['TABLE_NAME']),
            'select' => array('ID')
        ))->fetch();

        if ($rsHLBlock['ID']) {
            $rsResult = HL\HighloadBlockTable::delete($rsHLBlock["ID"]);
            if ($rsResult->isSuccess()) {
                return true;
            } else {
                $this->LAST_ERROR = $rsResult->getErrorMessages();
                return false;
            }
        } else {
            $this->LAST_ERROR = "Невозможно удалить тааблицу так как ее нет в базе";
        }
        return true;
    }

    private function ShowForm($type, $message, $buttonName='') {
        global $APPLICATION;
        $keys = array_keys($GLOBALS);
        for($i=0, $intCount = count($keys); $i < $intCount; $i++) {
            if($keys[$i]!='i' && $keys[$i]!='GLOBALS' && $keys[$i]!='strTitle' && $keys[$i]!='filepath') {
                global ${$keys[$i]};
            }
        }

        IncludeModuleLangFile(__DIR__.'/install.php');

        $APPLICATION->SetTitle(GetMessage('KBR_IBLOCK_MODULE_NAME'));
        include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
        echo CAdminMessage::ShowMessage(array('MESSAGE' => $message, 'TYPE' => $type));
        ?>
        <form action="<?= $APPLICATION->GetCurPage()?>" method="get">
            <p>
                <input type="hidden" name="lang" value="<? echo LANGUAGE_ID; ?>" />
                <input type="submit" value="<?= strlen($buttonName) ? $buttonName : GetMessage('MOD_BACK')?>" />
            </p>
        </form>
        <?
        include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
        die();
    }
}