<?php
namespace Kingbird;

class RedirectImport extends Redirect
{
    function __construct() {
        parent::__construct();

    }

    function add($from, $to) {
        $fields = [
            'UF_FROM' => $from,
            'UF_TO' => $to,
        ];

        if (!$this->_wrapper->getList([], $fields, ['limit' => 1])->fetch()) {
            return $this->_wrapper->add($fields);
        } else {
            return true;
        }
    }

}