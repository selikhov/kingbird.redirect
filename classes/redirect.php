<?php
namespace Kingbird;

class Redirect
{
    static protected $_instance;

    protected $_wrapper;
    protected $hlClass;

    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    protected function __construct() {
        \CModule::IncludeModule('highloadblock');
        $this->_wrapper = new HLWrapper("HLRedirect", 'hl_redirect');
        $this->hlClass = $this->_wrapper->getEntityDataClass();
    }

    public function getRedirectUrl($url) {
        if ($item = $this->_wrapper->getList(array(), array('UF_FROM' => $url), array('limit' => 1))->fetch()) {
            return $item['UF_TO'];
        }

        return false;
    }

    public function setRedirect($from, $to) {
        if (strlen($from) <= 0 || strlen($to) <= 0) {
            return false;
        }

        if (!$this->_wrapper->getList([], [
            'UF_FROM' => $from,
            'UF_TO' => $to
            ], ['limit' => 1])->fetch()
        ){
            $this->_wrapper->add([
                'UF_FROM' => $from,
                'UF_TO' => $to
            ]);
        }
    }
}
