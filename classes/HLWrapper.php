<?php
namespace Kingbird;

use \Bitrix\Main;
use \Bitrix\Highloadblock as HL;

class HLWrapper
{
    protected $arIblock = array();
    protected $entity;
    protected $entityDataClass;
    protected $hlClass;

    protected $arLoger = array();
    public $LAST_ERROR;

    function __construct($entityName, $tableName){
        $this->arIblock = array(
            'NAME' => $entityName,
            'TABLE_NAME' => $tableName,
        );
        $hlBlock = HL\HighloadBlockTable::getList(array(
            'filter' => array('TABLE_NAME' => $this->arIblock['TABLE_NAME']),
            'select' => array('ID'))
        )->fetch();

        if($hlBlock['ID']) {
            $this->arIblock['ID'] = $hlBlock['ID'];
            $this->entity = HL\HighloadBlockTable::compileEntity($this->arIblock);
            $this->entityDataClass = $this->entity->getDataClass();
        }else{
            throw new Main\ArgumentException(sprintf(
                'Invalid entity name `%s` or table name `%s`.', $entityName, $tableName
            ));
        }
        $this->hlClass = $this->getEntityDataClass();
    }

    function getEntity(){
        return $this->entity;
    }

    function getEntityDataClass(){
        return $this->entityDataClass;
    }

    function getList($arSort = array(), $arFilter = array(), $arNavParams = array(), $arSelect = array('*')){
        $entityDataClass = $this->entity->getDataClass();
        $hlParams = array();
        if($arSort){
            $hlParams['order'] = $arSort;
        }
        if($arFilter){
            $hlParams['filter'] = $arFilter;
        }
        if($arNavParams['nTopCount']){
            $hlParams['limit'] = $arNavParams['nTopCount'];
        }
        if($arNavParams['limit']){
            $hlParams['limit'] = $arNavParams['limit'];
        }
        if($arNavParams['offset']){
            $hlParams['offset'] = $arNavParams['offset'];
        }

        if($arSelect){
            $hlParams['select'] = $arSelect;
        }

        $highloadBlockCursor = $entityDataClass::getList($hlParams);

        return $highloadBlockCursor;
    }

    /**
     * Get items in array
     * @param array $arSort
     * @param array $arFilter
     * @param array $arNavParams
     * @param array $arSelect
     * @return array|bool
     */
    function getListArray($arSort = array(), $arFilter = array(), $arNavParams = array(), $arSelect = array('*'))
    {
        $highloadBlockCursor = self::getList($arSort, $arFilter, $arNavParams, $arSelect);
        $rsData = new CDBResult($highloadBlockCursor, $this->arIblock['TABLE_NAME']);
        while ($ob = $rsData->Fetch()) {
            $result[] = $ob;
        }
        return (!empty($result)) ? $result : false;
    }

    /**
     * Add item in highload block
     * @param array $arLoad
     * @return bool
     * @throws Exception
     */
    public function add(array $arLoad)
    {
        $entityDataClass = $this->entity->getDataClass();
        $result = $entityDataClass::add($arLoad);
        return ($result->isSuccess()) ? $result->getId() : false;
    }

    /**
     * Update highload item
     * @param $ID
     * @param array $arLoad
     * @return bool
     * @throws Exception
     */
    public function update($ID, array $arLoad)
    {
        $entityDataClass = $this->entity->getDataClass();
        return ($entityDataClass::update($ID, $arLoad)) ? true : false;
    }

    /**
     * Delete highload item
     * @param $ID
     * @return bool
     * @throws Exception
     */
    public function delete($ID)
    {
        $entityDataClass = $this->entity->getDataClass();
        return ($entityDataClass::delete($ID)) ? true : false;
    }


    protected function _createElement($arFields, $arCheckSectionFields = array()){
        $entityDataClass = $this->entity->getDataClass();
        $arItemHL = array();
        if(count($arCheckSectionFields) > 0) {
            $arFilter = array();
            if (count($arCheckSectionFields) > 0) {
                foreach ($arCheckSectionFields as $checkCode) {
                    $arFilter[$checkCode] = $arFields[$checkCode];
                }
            }

            if (count($arFilter) <= 0) {
                $this->LAST_ERROR = "Error empty HL Filter";
                return false;
            }
            $highloadBlockCursor = $entityDataClass::getList(array(
                'filter' => $arFilter,
                'select' => array('*'),
            ));
            $arItemHL = $highloadBlockCursor->fetch();
        }
        if ($arItemHL['ID'] && count($arCheckSectionFields) > 0) {
            $result = $entityDataClass::update($arItemHL['ID'], $arFields);
        } else {
            $result = $entityDataClass::add($arFields);
        }

        if ($result->isSuccess()) {
            $this->arLoger[] = "Успешно импортирован " . implode('/', $arFields);
            return $result->getId();
        } else {
            $this->LAST_ERROR = $result->getErrorMessages();
            return false;
        }
    }

}