<?php
$MESS ['KBR_MODULE_NAME'] = 'Редирект';
$MESS ['KBR_MODULE_DESCRIPTION'] = 'Модуль отслеживает редирект';
$MESS ['KBR_PARTNER_NAME'] = 'KingBird';
$MESS ['KBR_NEED_RIGHT_VER'] = 'Для установки данного решения необходима версия главного модуля #NEED# или выше.';
$MESS ['KBR_NEED_MODULES'] = 'Для установки данного решения необходимо наличие модуля #MODULE#.';